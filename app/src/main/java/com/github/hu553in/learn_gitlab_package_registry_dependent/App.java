package com.github.hu553in.learn_gitlab_package_registry_dependent;

import com.github.hu553in.learn_gitlab_package_registry.Library;

public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());

        Library library = new Library();
        library.someLibraryMethod();
    }
}
